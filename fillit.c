# include <stdio.h>
# include <string.h>
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <sys/types.h>

# define BUFFER_SIZE	545

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char const *s)
{
	int i;

	i = 0;
	if (s == NULL)
		return ;
	while (s[i] != '\0')
		ft_putchar(s[i++]);
}

int		ft_chrcnt(const char *str, char c)
{
	int		counter;

	counter = 0;
	if (!(*str) || !c)
		return (0);
	while (*str)
	{
		if (*str == c)
			counter++;
		str++;
	}
	return (counter);
}

void	error(void)
{
	ft_putstr("error\n");
	exit(0);
}

int		ft_tetri_counter(char *str)
{
	int		tetriminos;
	int		lines_number;

	lines_number = ft_chrcnt(str, '\n');
	tetriminos = (lines_number + 1) / 5;
	return (tetriminos);
}

int		ft_check_endline(char *str, int i)
{
	int		counter;

	counter = 0;
	while (str[i] != '\0')
	{
		while (str[i] == '.' || str[i] == '#')
		{
			counter++;
			i++;
		}
		if (str[i] == '\n')
		{
			counter++;
			if ((counter % 5) == 0 || (counter % 21) == 0)
				i++;
			else if ((counter % 5) != 0 || (counter % 21) != 0)
				return (0);
			if (counter == 21)
				counter = 0;
		}
	}
	return (1);
}

int		ft_valid_symbols(char *str)
{
	while (*str && ((*str == '.') || (*str == '#') || (*str == '\n')))
		str++;
	if (*str == '\0')
		return (1);
	return (0);
}

int		ft_valid_symbols_number(char *str)
{
	int		tetriminos;

	tetriminos = ft_tetri_counter(str);
	if ((ft_chrcnt(str, '.') == (tetriminos * 12)) &&
		(ft_chrcnt(str, '#') == (tetriminos * 4)))
		return (1);
	return (0);
}

int		ft_tetri_checker(char *str, int i)
{
	int		connect;
	int		cnt;
	int		j;

	connect = 0;
	cnt = 0;
	j = 0;
	while (str[i] != '\0')
	{
		while (((i + 1) % 21 != 0) && (str[i] == '.' || str[i] == '\n'))
		{
			j++;
			i++;
		}
		if (((i + 1) % 21 != 0) && str[i] == '#')
		{
			cnt++;
			if (str[i - 5] == '#' && j > 3)
                connect++;
			if (str[i + 1] == '#')
				connect++;
			if (str[i + 5] == '#' && j < 16)
				connect++;
			if (str[i - 1] == '#')
				connect++;
		}
		if ((i + 1) % 21 == 0)
		{
			if ((cnt == 4) && (connect != 6 && connect != 8))
				return (0);
			else if ((cnt == 4) && (connect == 6 || connect == 8))
			{
				i++;
				j++;
				if (!(ft_tetri_checker(str, i)))
					return (0);
			}
		}
		i++;
		j++;
	}
	return (1);
}

int		ft_checkmap(char *str)
{
	int		lines_number;

	lines_number = ft_chrcnt(str, '\n');
	if (ft_valid_symbols(str))
		if (ft_check_endline(str, 0))
			if (ft_valid_symbols_number(str))
				if (ft_tetri_checker(str, 0))
					return (1);
	return (0);
}

int		main(int argc, char **argv){
	int		fd;
	int		read_bytes;
	char	*buffer;

	if (argc != 2)
	{
		ft_putstr("usage: fillit source_file\n");
		exit(1);
	}
	if ((fd = open(argv[1], O_RDONLY)) < 0)
		error();
	buffer = (char *)malloc(BUFFER_SIZE + 1);
	if ((read_bytes = read(fd, buffer, BUFFER_SIZE + 1)) < 0)
		error();
	buffer[read_bytes] = '\0';
	if (!(ft_checkmap(buffer)))
		error();
	if (close(fd) != 0)
	{
		error();
		exit(1);
	}
	return (0);
}